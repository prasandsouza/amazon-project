import { ADD_CART } from "./cartType";

const initialState = {
    numOfProduct: 0
}

const addingcart = (state = initialState,action)=>{
    switch(action.type){
        case ADD_CART : return{
            ...state,
            numOfProduct : state.numOfProduct + 1
        }
        default : return state
    }
}

export default addingcart