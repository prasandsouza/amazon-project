import { createStore } from "redux";
import cardReducer from './cart/cardReducer'

const store = createStore(cardReducer)

export default store