import React, { Component } from 'react'
import '../App.css'
import logo from '../images/amazonlogo.png'
export class Footer extends Component {
    render() {
        return (
            <div className='container-fluid mt-4'>
                <div className='row'>
                    <div className='col-12 back-to-top p-2'>
                        <a href='#' className='text-center text-decoration-none text-light '> Back to top </a>
                    </div>

                </div>
                <div className='row'>
                    <div className=' d-flex flex-row second-footer w-100 text-light justify-content-evenly p-5 '>
                        <div className='d-flex flex-column text-start'>
                            <h6>Get to know us</h6>
                            <ul className='list-unstyled '>
                                <li className=''> About Us</li>
                                <li> Careers</li>
                                <li> Press Realease</li>
                                <li> Amazon Cares</li>
                                <li> Gift a Smile</li>
                                <li>Amazon Science</li>
                            </ul>
                        </div>
                        <div className='d-flex flex-column text-start'>
                            <h6> Conect with Us</h6>
                            <ul className='list-unstyled'>
                                <li> FaceBook </li>
                                <li> Twitter</li>
                                <li> Instagram</li>
                            </ul>
                        </div>
                        <div className='d-flex flex-column text-start'>
                            <h6> Make Money With Us</h6>
                            <ul className='list-unstyled text-start'>
                                <li> sell on Amazon</li>
                                <li> Sell under Amazon accelerator </li>
                                <li> Amazon Global selling </li>
                                <li> Become an Affiliate </li>
                                <li> Fulfilment by Amazon</li>
                                <li> Advertize your Product </li>
                                <li> Amazon Pay on Merchent</li>
                            </ul>
                        </div>
                        <div className='d-flex flex-column text-start'>
                            <h6> Let Us Help You</h6>
                            <ul className='list-unstyled text-start'>
                                <li> COVID-19 and Amazon </li>
                                <li> Your Account </li>
                                <li> Return Centre </li>
                                <li> 100% Purchase Protection </li>
                                <li> Amazon App Download </li>
                                <li> Amazon Assistant Download </li>
                                <li> Help </li>
                            </ul>
                        </div>
                    </div>

                    <div className='second-footer text-light'>
                        <div className='d-flex flex-row justify-content-center mb-6'>
                            <img src={logo} width='70px' height='25px' className='my-2' />
                            <div className="dropdown mx-5">
                                <a className="btn btn-secondary dropdown-toggle btn-outline-secondary text-dark" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Language
                                </a>

                                <ul className="dropdown-menu">
                                    <li><a className="dropdown-item" href="#">English</a></li>
                                    <li><a className="dropdown-item" href="#">Hindi</a></li>
                                    <li><a className="dropdown-item" href="#">Kannada</a></li>
                                </ul>
                            </div>
                        </div>
                        <p className='my-2'>
                            <span className='mx-2'>Australia</span>
                            <span className='mx-2'>Brazil</span>
                            <span className='mx-2'> Canada </span>
                            <span className='mx-2'> China  </span>
                            <span className='mx-2'>  France  </span>
                            <span className='mx-2'> Germany </span>
                            <span className='mx-2'> Italy </span>
                            <span className='mx-2'> Japan </span>
                            <span className='mx-2'> Mexico </span>
                            <span className='mx-2'> Netherland </span>
                            <span className='mx-2'> Poland </span>
                            <span className='mx-2'> Singapore </span>
                            <span className='mx-2'> Spain </span>
                            <span className='mx-2'> Turkey </span>
                            <span className='mx-2'> UAE </span>
                            <span className='mx-2'> UK </span>
                            <span className='mx-2'> USA </span>
                        </p>
                    </div>
                    <div className='third-footer d-flex flex-column text-light'>
                        <div className='d-flex flex-row  mt-3 text-start mx-5'>
                            <a className='w-25 text-decoration-none text-light mx-5'>
                                <h6> AdeBooks </h6>
                                <p> Books, art & collection</p>
                            </a>
                            <a className='w-25 text-decoration-none text-light'>
                                <h6> Amazon Web services </h6>
                                <p> Books, art & collection</p>
                            </a>
                            <a className='w-25 text-decoration-none text-light'>
                                <h6> Audible </h6>
                                <p> Download</p>
                            </a>
                            <a className='w-25 text-decoration-none text-light'>
                                <h6> DPreview </h6>
                                <p> digital photography</p>
                            </a>
                        </div>
                        <div className='d-flex flex-row justify-content-around mt-3 mx-5 text-start'>
                            <a className='w-25 text-decoration-none text-light mx-5'>
                                <h6> Shopbop </h6>
                                <p> Designer</p>
                            </a>
                            <a className='w-25 text-decoration-none text-light'>
                                <h6> Amazon Business </h6>
                                <p> everything for your business</p>
                            </a>
                            <a className='w-25 text-decoration-none text-light'>
                                <h6> Prime Now </h6>
                                <p> 2 hour delivery</p>
                            </a>
                            <a className='w-25 text-decoration-none text-light'>
                                <h6> Amazon Prime Music </h6>
                                <p> 90 million ad free songs</p>
                            </a>
                        </div>
                        <div>
                            <p className='mt-5'>
                                <span className='mx-2'>Conditions of Use & Sale </span>
                                <span className='mx-2'> Privacy Notice </span>
                                <span className='mx-2'> Interest-Based Ads </span>
                                <span className='mx-2'> © 1996-2022, Amazon.com, Inc. or its affiliates </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Footer