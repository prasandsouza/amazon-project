import React, { Component } from 'react'

export class Loader extends Component {
    render() {
        return (
            <div>
                <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
            </div>
        )
    }
}

export default Loader