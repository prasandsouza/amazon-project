import React, { Component } from 'react'
import axios from 'axios'
import Loader from './Loader'
import './loader.css'
class Api extends Component {
    constructor(props) {
        super(props)
        this.appstate = {
            LOADING: 'loading',
            LOADED: 'loaded',
            ERROR: 'error'
        }

        this.state = {
            status: this.appstate.LOADING,
            data: []
        }
    }
    componentDidMount = () => {
        axios.get('https://fakestoreapi.com/products')
            .then((event) => {
                this.setState({
                    data: event.data,
                    status: this.appstate.LOADED
                })
            }).catch((err) => {
                this.setState({
                    status: this.appstate.ERROR
                })
            })
    }
    render() {
        console.log(this.state.data)
        return (
            <div>
                {this.state.status === this.appstate.LOADING ? <Loader /> : ''}
                {this.state.status === this.appstate.ERROR ? <h1> error while fetching data</h1> : ''}
                {this.state.status === this.appstate.LOADED ?
                    <div> {this.state.data.length > 0 ?
                        this.state.data.map((values) => {
                            return (
                                <div>
                                </div>
                            )
                        })
                        : <div>  <h4> no products are availble</h4> </div>} </div>
                    : ''}
            </div>
        )
    }
}
export default Api