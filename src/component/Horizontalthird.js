import React, { Component } from 'react'
import './horizontal.css'
export class Horizontalthird extends Component {
  render() {
    return (
        <div>

        <div className='container-1 mt-5 text-start p-2'>
            <h4 className='mt-3 mx-2'> Best Sellers in Sports, Fitness & Outdoors</h4>
            <div className="horizontal mt-5">
                <img className='mx-2 content mt-4 mb-4'  src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/51zC8UjgTwL._AC_SY200_.jpg' ></img>
                <img className='mx-2 content mt-4 mb-4 img' src=' https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/61vxZ9hqRjL._AC_SY200_.jpg' ></img>
                <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/61+QXJBgIgL._AC_SY200_.jpg '></img>
                <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/81iZzKTg3RL._AC_SY200_.jpg'></img>
                <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/61XPTRJZcCL._AC_SY200_.jpg'></img>
                <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/81Qft4sWORL._AC_SY200_.jpg'></img>
                <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/71zfa+r8CLL._AC_SY200_.jpg'></img>
                <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/51HZ1xl03VS._AC_SY200_.jpg'></img>
                <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/81g87b5bzLL._AC_SY200_.jpg'></img>
                <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/61DteV3hXqL._AC_SY200_.jpg'></img>
                <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/61U5fvYnciL._AC_SY200_.jpg'></img>
                <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/61nnUnnwDkL._AC_SY200_.jpg'></img>
                <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/71HNPjXWyQL._AC_SY200_.jpg'></img>
                <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/71AF5VeQrJL._AC_SY200_.jpg'></img>
            </div>
        </div>
    </div>
    )
  }
}

export default Horizontalthird