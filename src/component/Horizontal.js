import React from 'react'
import './horizontal.css'
import { connect } from 'react-redux'
import { addcart } from '../redux'

function Horizontal(props) {
    return (
        <div>
            <div className='container-1 mt-5 text-start p-2'>
                <h4 className='mt-3 mx-2'> Deal of the day</h4>
                <div className="horizontal mt-5">
                    <div>
                        <img className='mx-2 content mt-4 mb-4' alt='product' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/410UOtOGvsL._AC_SY200_.jpg' />
                        <div className='d-flex flex-row'>
                            <p className='textback-color text-light w-25 mx-3'>35% off</p>
                            <p className='text-danger'> deal of the day</p>
                        </div>
                        <button className='btn btn-warning rounded-5 btn-sm my-1 mx-3' onClick={props.addcart}> add to cart </button>
                    </div>
                    <div>
                        <img className='mx-2 content mt-4 mb-4'  alt='product' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/414057taYKL._AC_SY200_.jpg' />
                        <div className='d-flex flex-row'>
                            <p className='textback-color text-light w-25 mx-3'> &#8377;2000 off</p>
                            <p className='text-danger'> deal of the day</p>
                        </div>
                        <button className='btn btn-warning rounded-5 btn-sm my-1 mx-3' onClick={props.addcart}> add to cart </button>
                    </div>
                    <div>
                        <img className='mx-2 content mt-4 mb-4'  alt='product'  src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/310oMFhuDqL._AC_SY200_.jpg' />
                        <div className='d-flex flex-row'>
                            <p className='textback-color text-light w-25 mx-3'> &#8377;2500 off</p>
                            <p className='text-danger'> deal of the day</p>
                        </div>
                        <button className='btn btn-warning rounded-5 btn-sm my-1 mx-3' onClick={props.addcart}> add to cart </button>
                    </div>
                    <div>
                        <img className='mx-2 content mt-4 mb-4'  alt='product'  src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/41+sLJHyA8L._AC_SY200_.jpg' />
                        <div className='d-flex flex-row'>
                            <p className='textback-color text-light w-25 mx-3'> &#8377;500 off</p>
                            <p className='text-danger'> deal of the day</p>
                        </div>
                        <button className='btn btn-warning rounded-5 btn-sm my-1 mx-3' onClick={props.addcart}> add to cart </button>
                    </div>
                    <div>
                        <img className='mx-2 content mt-4 mb-4'  alt='product'  src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/21qvad6Z1oL._AC_SY200_.jpg' />
                        <div className='d-flex flex-row'>
                            <p className='textback-color text-light w-25 mx-3'> &#8377;300 off</p>
                            <p className='text-danger'> deal of the day</p>
                        </div>
                        <button className='btn btn-warning rounded-5 btn-sm mx-3 my-1' onClick={props.addcart}> add to cart </button>
                    </div>
                    <div>
                        <img className='mx-2 content mt-4 mb-4'   alt='product' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/21Y3xVnL0BL._AC_SY200_.jpg' />
                        <div className='d-flex flex-row'>
                            <p className='textback-color text-light w-25 mx-3'> 50% off</p>
                            <p className='text-danger'> deal of the day</p>
                        </div>
                        <button className='btn btn-warning rounded-5 btn-sm mx-3 my-1' onClick={props.addcart}> add to cart </button>
                    </div>
                    <div>
                        <img className='mx-2 content mt-4 mb-4'  alt='product'  src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/410ru6PdiCL._AC_SY200_.jpg' />
                        <div className='d-flex flex-row'>
                            <p className='textback-color text-light w-25 mx-3'> 100% off</p>
                            <p className='text-danger'> deal of the day</p>
                        </div>
                        <button className='btn btn-warning rounded-5 btn-sm mx-3 my-1' onClick={props.addcart}> add to cart </button>
                    </div>
                    <div>
                        <img className='mx-2 content mt-4 mb-4'   alt='product' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/51xLCyz8RzL._AC_SY200_.jpg' />
                        <div className='d-flex flex-row'>
                            <p className='textback-color text-light w-25 mx-3'> &#8377;150 off</p>
                            <p className='text-danger'> deal of the day</p>
                        </div>
                        <button className='btn btn-warning rounded-5 btn-sm mx-3 my-1'onClick={props.addcart}> add to cart </button>
                    </div>
                    <div>
                        <img className='mx-2 content mt-4 mb-4'  alt='product'  src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/31yz6TdlRAS._AC_SY200_.jpg' />
                        <div className='d-flex flex-row'>
                            <p className='textback-color text-light w-25 mx-3'> &#8377;200 off</p>
                            <p className='text-danger'> deal of the day</p>
                        </div>
                        <button className='btn btn-warning rounded-5 btn-sm mx-3 my-1'onClick={props.addcart}> add to cart </button>
                    </div>
                </div>

            </div>
        </div>
    )
}



const mapDispatchToProps = dispatch =>{
    return {
        addcart : ()=>{
            dispatch(addcart())
        }
    }
}

export default connect(null,mapDispatchToProps)(Horizontal)