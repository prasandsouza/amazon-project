import React from 'react'
import './horizontal.css'
import harpik from '../images/harpik.jpg'

function Horizontal() {
    return (
        <div>

            <div className='container-1 mt-5 text-start p-2'>
                <h4 className='mt-3 mx-2'> Frequently repurchased in Home</h4>
                <div className="horizontal mt-5">
                    <img className='mx-2 content mt-4 mb-4'  src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/81NoZal4e1L._AC_SY200_.jpg' ></img>
                    <img className='mx-2 content mt-4 mb-4' src=' https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/71Ed4esCJXL._AC_SY200_.jpg'></img>
                    <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/61dxH7RrbZL._AC_SY200_.jpg'></img>
                    <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/61tJQVLMNCL._AC_SY200_.jpg'></img>
                    <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/61RsBQWuKBL._AC_SY200_.jpg'></img>
                    <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/81C4M6e3-1L._AC_SY200_.jpg'></img>
                    <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/516ej6vg9LL._AC_SY200_.jpg'></img>
                    <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/81K0DtvReLL._AC_SY200_.jpg'></img>
                    <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/81-BZ3CuNDL._AC_SY200_.jpg'></img>
                    <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/81kLfkRGC2L._AC_SY200_.jpg'></img>
                    <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/716GRSUDYdL._AC_SY200_.jpg'></img>
                    <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/51vlZGP36sL._AC_SY200_.jpg'></img>
                    <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/8106zYzdc8L._AC_SY200_.jpg'></img>
                    <img className='mx-2 content mt-4 mb-4' src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/61kh9+7bZAL._AC_SY200_.jpg'></img>

                </div>

            </div>
        </div>
    )
}

export default Horizontal