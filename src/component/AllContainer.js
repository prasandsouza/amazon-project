import Cards from './Cards';
import Container1 from './Container1';
import Fourthcard from './Fourthcard';
import Horizontal from './Horizontal';
import Horizontalsecond from './Horizontalsecond';
import Horizontalthird from './Horizontalthird';

function AllContainer() {
    return (
        <div className="App">
            <Container1 />
            <Cards />
            <Horizontal />
            <Horizontalsecond />
            <Horizontalthird />
            <Fourthcard />
        </div>
    );
}

export default AllContainer;
