import React, { Component } from 'react'
import logo from '../images/amazonlogo.png'
import flag from '../images/indianflag.png'
import cart from '../images/cart.svg'
import search from '../images/search.png'
import { connect } from 'react-redux'
import { addcart } from '../redux'
import { Link } from 'react-router-dom'


class Header extends Component {
    render() {
        return (
            <>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark d-flex align-items-center sticky-top">
                    <div className="container-fluid">
                        <img src={logo} alt='imagelogo' width="80" height="24" />
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav me-auto d-flex align-items-center w-100">
                                <li className="nav-item">
                                    <a className="nav-link active" aria-current="page" href="/">Select your address</a>
                                </li>
                                <div className="input-group h-25">
                                    <button className="btn btn-outline-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">All</button>
                                    <ul className="dropdown-menu bg-dark">
                                        <li><button className="dropdown-item text-light" type="button"> All categories</button></li>
                                        <li><button className="dropdown-item text-light" type="button">alexa skill</button></li>
                                        <li><button className="dropdown-item text-light" type="button">amazon Devices</button></li>
                                        <li><button className="dropdown-item text-light" type="button">Amazon fashion</button></li>
                                        <li><button className="dropdown-item text-light" type="button">Amazon Fresh</button></li>
                                        <li><button className="dropdown-item text-light" type="button">Amazon Pharmacy</button></li>
                                        <li><button className="dropdown-item text-light" type="button">Appliances</button></li>
                                        <li><button className="dropdown-item text-light" type="button">Apps & Games</button></li>
                                        <li><button className="dropdown-item text-light" type="button">Baby</button></li>
                                        <li><button className="dropdown-item text-light" type="button">Beauty</button></li>
                                        <li><button className="dropdown-item text-light" type="button">Books</button></li>
                                        <li><button className="dropdown-item text-light" type="button">car & Motorbike</button></li>
                                        <li><button className="dropdown-item text-light" type="button">Clothing</button></li>
                                        <li><button className="dropdown-item text-light" type="button">furniture</button></li>
                                    </ul>
                                    <input type="text" className="form-control" aria-label="Text input with dropdown button" />
                                    <button className="btn btn-outline-secondary" type="button" id="button-addon2">
                                        <img src={search} width='20px' height='20px'></img>
                                    </button>
                                </div>
                                <li className="nav-item dropdown w-25">
                                    <div className='container w-50'>
                                        <a className="nav-link dropdown-toggle w-50" href="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            <img src={flag} alt='imagelogo' width="20" height="20" />
                                        </a>
                                        <ul className="dropdown-menu">
                                            <li><a class="dropdown-item" href="#">India</a></li>
                                            <li><a class="dropdown-item" href="#">Russia</a></li>
                                            <li><a class="dropdown-item" href="#">America</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle active" href="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        Account & List
                                    </a>
                                    <ul className="dropdown-menu">
                                        <li><a className="dropdown-item" href="/">login</a></li>
                                        <li><a className="dropdown-item" href="/">sign-up</a></li>
                                        <li><hr className="dropdown-divider" /></li>
                                        <Link to='src/component/apifiles/ApiData.js'>
                                            <li><a className="dropdown-item" href="/">browse product</a></li>
                                        </Link>
                                    </ul>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/">Returns&Orders</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/">
                                        <i className="fa-solid fa-cart-shopping"></i>
                                    </a>
                                </li>
                                <li>
                                    <h6 className='text-light'>{this.props.numberofproduct}</h6>
                                </li>
                                <li>
                                    <img src={cart} alt='cart' width="30" height="30" />
                                    <p className='fs-6 text-light'> cart</p>
                                </li>

                            </ul>
                        </div>
                    </div>
                </nav>
            </>
        )
    }
}
const mapStatetoProps = state => {
    return {
        numberofproduct: state.numOfProduct
    }
}
export default connect(mapStatetoProps)(Header)
