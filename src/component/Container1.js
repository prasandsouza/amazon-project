import React, { Component } from 'react'
import home1 from '../images/home1.jpg'
import home2 from '../images/home2.jpg'
import home3 from '../images/home3.jpg'
import home4 from '../images/home4.jpg'

export class Container1 extends Component {
    render() {
        return (
            <div>
                <div id="carouselExampleControls" class="carousel slide " data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src={home1} class="d-block w-100" alt="..." />
                        </div>
                        <div class="carousel-item">
                            <img src={home2} class="d-block w-100" alt="..." />
                        </div>
                        <div class="carousel-item">
                            <img src={home3} class="d-block w-100" alt="..." />
                        </div>
                        <div class="carousel-item">
                            <img src={home4} class="d-block w-100" alt="..." />
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>


            </div>
        )
    }
}

export default Container1