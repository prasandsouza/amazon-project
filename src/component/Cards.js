import React, { Component } from 'react'
import '../App.css'
import ac from '../images/fridge.jpg'
import fridge from '../images/ac.jpg'
import oven from '../images/oven.jpg'
import wash from '../images/washmachine.jpg'
import fest from '../images/season.jpg'
import laptop from '../images/laptop.jpg'
import laptopsecond from '../images/laptop1.jpg'




export class Cards extends Component {
    render() {
        return (
            <div className='d-flex flex-column item'>

                <div className='d-flex flex-row justify-content-around'>
                    <div className='d-flex flex-column p-3 bg-light'>
                        <h5>Top picks for your home</h5>
                        <div className='d-flex flex-row'>
                            <div className='d-flex flex-column m-2'>
                                <img src={ac} alt='ac' width='110px' height='100px' />
                                <p> Ac's</p>
                            </div>
                            <div className='d-flex flex-column m-2'>
                                <img src={fridge} alt='ac' width='110px' height='100px' />
                                <p> Refridgerator</p>
                            </div>
                        </div>
                        <div className='d-flex flex-row'>
                            <div className='d-flex flex-column m-2'>
                                <img src={oven} alt='ac' width='110px' height='100px' />
                                <p> MicroWaves</p>
                            </div>
                            <div className='d-flex flex-column m-2'>
                                <img src={wash} alt='ac' width='110px' height='100px' />
                                <p> Washing machines</p>
                            </div>
                        </div>
                        <a href='#' className='text-primary text-decoration-none text-start mx-2'> See more </a>
                    </div>

                    <div className='d-flex flex-column p-3 bg-light p-3'>
                        <h5 className='fs-6'> Now India will say "Got it on Amazon"</h5>
                        <img src={fest} alt='ac' width='275px' height='280px' className='mt-4' />
                        <a href='#' className='text-primary text-decoration-none text-start mx-2 mt-4'> See all upcoming offers </a>
                    </div>

                    <div className='d-flex flex-column p-3 bg-light p-3'>
                        <h5 className='fs-6'> Up to 70% off | Clearance store</h5>
                        <img src={laptop} alt='ac' width='275px' height='300px' className='mt-2' />
                        <a href='#' className='text-primary text-decoration-none text-start mx-2 mt-4'> See all upcoming offers </a>
                    </div>

                    <div className='d-flex flex-column p-3 bg-light p-3'>
                        <h5 className='fs-6'> Sign in for your best experience</h5>
                        <button className='btn btn-primary'> Sign in Securely</button>
                        <img src={laptopsecond} alt='ac' width='250px' height='280px' className='mt-4' />
                    </div>

                </div>

                <div className='d-flex flex-row justify-content-around mt-5'>
                    <div className='d-flex flex-column p-3 bg-light p-3'>
                        <h5 className='fs-6'> Revamp your home in style</h5>
                        <div className='d-flex flex-row'>
                            <div className='d-flex flex-column m-2'>
                                <img src='https://images-eu.ssl-images-amazon.com/images/W/WEBP_402378-T1/images/G/31/IMG20/Home/2021/GW/MSO/April/372x232_1_Low._SY116_CB670263840_.jpg' alt='ac' width='110px' height='100px' />
                                <p > BedSheets &more</p>
                            </div>
                            <div className='d-flex flex-column m-2'>
                                <img src='https://images-eu.ssl-images-amazon.com/images/W/WEBP_402378-T1/images/G/31/IMG20/Home/2021/GW/MSO/April/372x232_2_Low._SY116_CB670263840_.jpg' alt='ac' width='110px' height='100px' />
                                <p> Refridgerator</p>
                            </div>
                        </div>
                        <div className='d-flex flex-row'>
                            <div className='d-flex flex-column m-2'>
                                <img src='https://images-eu.ssl-images-amazon.com/images/W/WEBP_402378-T1/images/G/31/IMG20/Home/2021/GW/MSO/April/372x232_3_Low._SY116_CB670263840_.jpg' alt='ac' width='110px' height='100px' />
                                <p> MicroWaves</p>
                            </div>
                            <div className='d-flex flex-column m-2'>
                                <img src='https://images-eu.ssl-images-amazon.com/images/W/WEBP_402378-T1/images/G/31/IMG20/Home/2021/GW/MSO/April/372x232_4_Low._SY116_CB670263840_.jpg' alt='ac' width='110px' height='100px' />
                                <p> Washing machines</p>
                            </div>
                        </div>
                        <a href='#' className='text-primary text-decoration-none text-start mx-2'> Explore all </a>
                    </div>
                    <div className='d-flex flex-column p-3 bg-light p-3'>
                        <h5>Shop by Category</h5>
                        <div className='d-flex flex-row'>
                            <div className='d-flex flex-column m-2'>
                                <img src='https://images-eu.ssl-images-amazon.com/images/W/WEBP_402378-T1/images/G/31/img19/2020/PC/Fresh._SY116_CB431401553_.jpg' alt='ac' width='120px' height='100px' />
                                <p> fesr</p>
                            </div>
                            <div className='d-flex flex-column m-2'>
                                <img src='https://images-eu.ssl-images-amazon.com/images/W/WEBP_402378-T1/images/G/31/img19/2020/PC/Mobile._SY116_CB431401553_.jpg' alt='ac' width='120px' height='100px' />
                                <p> Mobile</p>
                            </div>
                        </div>
                        <div className='d-flex flex-row'>
                            <div className='d-flex flex-column m-2'>
                                <img src='https://images-eu.ssl-images-amazon.com/images/W/WEBP_402378-T1/images/G/31/img19/2020/PC/Fashion._SY116_CB431401553_.jpg' alt='ac' width='120px' height='100px' />
                                <p> Fashion</p>
                            </div>
                            <div className='d-flex flex-column m-2'>
                                <img src='https://images-eu.ssl-images-amazon.com/images/W/WEBP_402378-T1/images/G/31/img19/2020/PC/Electronic._SY116_CB431401553_.jpg' alt='ac' width='120px' height='100px' />
                                <p> Electronic</p>
                            </div>
                        </div>
                        <a href='#' className='text-primary text-decoration-none text-start mx-2'> See more </a>
                    </div>
                    <div className='d-flex flex-column p-3 bg-light p-3'>
                        <h5>Amazon Pay | book your<br></br> travel tickets</h5>
                        <div className='d-flex flex-row'>
                            <div className='d-flex flex-column m-2'>
                                <img src='https://images-eu.ssl-images-amazon.com/images/W/WEBP_402378-T1/images/G/31/img16/malar/RevAprGW/Flight_1._SY116_CB636958158_.jpg' alt='ac' width='110px' height='100px' />
                                <p> Flight Ticket</p>
                            </div>
                            <div className='d-flex flex-column m-2'>
                                <img src='https://images-eu.ssl-images-amazon.com/images/W/WEBP_402378-T1/images/G/31/img16/malar/RevAprGW/Bus_3._SY116_CB636958158_.jpg' alt='ac' width='110px' height='100px' />
                                <p> Bus ticket</p>
                            </div>
                        </div>
                        <div className='d-flex flex-row'>
                            <div className='d-flex flex-column m-2'>
                                <img src='https://images-eu.ssl-images-amazon.com/images/W/WEBP_402378-T1/images/G/31/img16/malar/RevAprGW/Train_2._SY116_CB636958158_.jpg' alt='ac' width='110px' height='100px' />
                                <p> Train ticket</p>
                            </div>
                            <div className='d-flex flex-column m-2'>
                                <img src='https://images-eu.ssl-images-amazon.com/images/W/WEBP_402378-T1/images/G/31/img16/malar/RevAprGW/Travel_accessories_4._SY116_CB636958158_.jpg' alt='ac' width='110px' height='100px' />
                                <p> Essential travel</p>
                            </div>
                        </div>
                        <a href='#' className='text-primary text-decoration-none text-start mx-2'> See more </a>
                    </div>
                    <div className='d-flex flex-column p-3 bg-light p-3'>
                        <h5> Upgrade your home | <br></br> Amazon Brands</h5>
                        <div className='d-flex flex-row'>
                            <div className='d-flex flex-column m-2'>
                                <img src='https://images-eu.ssl-images-amazon.com/images/W/WEBP_402378-T1/images/G/31/img21/AmazonBrands/GW/TV_1x._SY116_CB627443840_.jpg' alt='ac' width='100px' height='100px' />
                                <p> Tv</p>
                            </div>
                            <div className='d-flex flex-column m-2'>
                                <img src='https://images-eu.ssl-images-amazon.com/images/W/WEBP_402378-T1/images/G/31/AmazonSmallBusinessDay/PrivateBrands/GW20/GW_Desktop_QC_Appliances_1X_V2_3._SY116_CB636581536_.jpg' alt='ac' width='100px' height='100px' />
                                <p> Appliences</p>
                            </div>
                        </div>
                        <div className='d-flex flex-row'>
                            <div className='d-flex flex-column m-2'>
                                <img src='https://images-eu.ssl-images-amazon.com/images/W/WEBP_402378-T1/images/G/31/img21/AmazonBrands/GW/Furn_1x._SY116_CB627275939_.jpg' alt='ac' width='100px' height='100px' />
                                <p> Furninture</p>
                            </div>
                            <div className='d-flex flex-column m-2'>
                                <img src='https://images-eu.ssl-images-amazon.com/images/W/WEBP_402378-T1/images/G/31/img21/AmazonBrands/GW/Kitchen_1x._SY116_CB627276315_.jpg' alt='ac' width='100px' height='100px' />
                                <p> kitchen product</p>
                            </div>
                        </div>
                        <a href='#' className='text-primary text-decoration-none text-start mx-2'> See more </a>
                    </div>


                </div>
            </div>

        )
    }
}

export default Cards