import React, { Component } from 'react'
import logo from '../images/amazonlogo.png'
import flag from '../images/indianflag.png'
import cart from '../images/cart.svg'
export class Navbar extends Component {
    render() {
        return (
            <div>
                <nav class="navbar navbar-dark bg-dark d-flex">
                    <div class="container-fluid d-flex flex-row justifycontent-start">
                        <a class="navbar-brand" href="#">
                            <img src={logo} alt='imagelogo' width="80" height="24" />
                        </a>
                        <div className='text-light '>
                            <p className='fs-6'>Select your address</p>
                        </div>
                        <div className='input-group'>
                            <div className="btn-group">
                                <button type="button" className="btn btn-secondary dropdown-toggle" data-bs-toggle="dropdown" data-bs-display="static" aria-expanded="false">
                                    All
                                </button>
                                <ul className="dropdown-menu dropdown-menu-lg-end bg-dark text-light">
                                    <li><button className="dropdown-item text-light" type="button"> All categories</button></li>
                                    <li><button className="dropdown-item text-light" type="button">alexa skill</button></li>
                                    <li><button className="dropdown-item text-light" type="button">amazon Devices</button></li>
                                    <li><button className="dropdown-item text-light" type="button">Amazon fashion</button></li>
                                    <li><button className="dropdown-item text-light" type="button">Amazon Fresh</button></li>
                                    <li><button className="dropdown-item text-light" type="button">Amazon Pharmacy</button></li>
                                    <li><button className="dropdown-item text-light" type="button">Appliances</button></li>
                                    <li><button className="dropdown-item text-light" type="button">Apps & Games</button></li>
                                    <li><button className="dropdown-item text-light" type="button">Baby</button></li>
                                    <li><button className="dropdown-item text-light" type="button">Beauty</button></li>
                                    <li><button className="dropdown-item text-light" type="button">Books</button></li>
                                    <li><button className="dropdown-item text-light" type="button">car & Motorbike</button></li>
                                    <li><button className="dropdown-item text-light" type="button">Clothing</button></li>
                                    <li><button className="dropdown-item text-light" type="button">furniture</button></li>
                                </ul>
                            </div>
                            <form class="d-flex" role="search">
                                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
                            </form>
                        </div>
                        <div>
                            <p className='text-light'> English</p>
                            <img src={flag} alt='imagelogo' width="20" height="20" />
                            <div class="btn-group">
                                <button class="btn btn-secondary dropdown-toggle"  type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                </button>
                                
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="#">India</a></li>
                                    <li><a class="dropdown-item" href="#">Russia</a></li>
                                    <li><a class="dropdown-item" href="#">America</a></li>
                                </ul>
                            </div>

                        </div>
                        <div>
                            <p className='text-light'> Hello, sign in</p>
                            <p className='text-light'> Account & Lists</p>
                        </div>
                        <div>
                            <p className='text-light'> return</p>
                            <p className='text-light'> & Orders</p>
                        </div>
                        <div>
                        <img src={cart} alt='cart' width="40" height="40" />
                        <p className='fs-6 text-light'> cart</p>
                        </div>
                    </div>
                </nav>
            </div>
        )
    }
}

export default Navbar