import './App.css';
import Cards from './component/Cards';
import Footer from './component/Footer';
import Header from './component/Header';
import { Provider } from 'react-redux'
import store from './redux/store';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import AllContainer from './component/AllContainer';
import ApiData from './component/apifiles/ApiData';

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <BrowserRouter>
          <Header />
          <Routes>
            <Route exact path='/' element={<AllContainer />} />
            <Route exact path='src/component/apifiles/ApiData.js' element={<ApiData/>}/>
          </Routes>
        </BrowserRouter>
        <Footer />
      </Provider>
    </div>
  );
}

export default App;
